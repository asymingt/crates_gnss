% Open truth data
g = importdata('mobile-novatel.pos', ' ', 18);
g.textdata = g.textdata(19:end,:);
g = [datenum(strcat(g.textdata(:,1),{' '},g.textdata(:,2))), g.data(:,1:3)];

% Open estimated data
e = importdata('mobile-nvs.pos', ' ', 14);
e.textdata = e.textdata(15:end,:);
e = [datenum(strcat(e.textdata(:,1),{' '},e.textdata(:,2))), e.data(:,1:3)];

% Interpolate along truth axis
t = g(:,1);
e = [t,interp1(e(:,1),e(:,2),t),interp1(e(:,1),e(:,3),t),interp1(e(:,1),e(:,4),t)];

% Limit data range
m = 350;
n = length(t) - 700;

% Plot the mm error
err = (e - g) * 1000;

% Plot the birds eye view
h = figure; hold on;
plot(g(m:n,3),g(m:n,2),'k');
plot(e(m:n,3),e(m:n,2),'r');
grid on;
ylabel('ECEF meters Y');
xlabel('ECEF meters X');

% Plot the error along each axis
h = figure;
subplot(3,1,1); hold on;
plot(t(m:n),err(m:n,2),'k');
subplot(3,1,2); hold on;
plot(t(m:n),err(m:n,3),'k');
subplot(3,1,3); hold on;
plot(t(m:n),err(m:n,4),'k');

// STL stuff
#include <map>
#include <vector>
#include <iostream>
#include <sstream>

// Required for the maths functions
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/sensors/sensors.hh>

// Cubic spline interpolation
#include "spline.h"

// GNSS processing functionality
extern "C"
{
  // Include RTKLIB
  #include <rtklib.h>
}

namespace gazebo
{
  // Solution state
  typedef enum
  {
    STATE_DATE = 0,
    STATE_TIME,
    STATE_X,
    STATE_Y,
    STATE_Z,
    STATE_Q,
    STATE_NS,
    STATE_SX,
    STATE_SY,
    STATE_SZ,
    STATE_SDX,
    STATE_SDY,
    STATE_SDZ,
    STATE_AGE,
    STATE_RATIO,
    NUM_STATES
  }
  SolutionState;

  // Structure of a GNSS observable
  typedef struct 
  {
    int sys;                // System code
    int prn_min;            // Min PRN
    int prn_max;            // Max PRN
    common::Time nav_last;  // NAV: Last update time
    common::Time nav_wait;  // NAV: Wait interval
    int rtcm_obs;           // RTCM3 code for observation message
    int rtcm_nav;           // RTCM3 code for navigation message
    int code[3];            // Codes
    double freq[3];         // Frequencies
    double bias[3];         // Biases
  }
  GNSS;

  // Receiver plugin
  class GpsPlugin : public SensorPlugin
  {
    // Current solution state
    private: char state_buff[512];        // Buffer with response from RTKLIB
    private: int state_ptr;               // Buffer length
    private: double lastupdate;           // Last update time

    // References
    protected: physics::WorldPtr worldPtr;
    protected: sensors::GpsSensorPtr parentSensor;
    protected: event::ConnectionPtr conPtr;

    // OBS and NAV data for GNSS
    protected: bool mask, debug;           // Mask NAV below horizon
    protected: nav_t bnav;                 // Bradcast navigation data
    protected: nav_t pnav;                 // Bradcast navigation data
    protected: rtcm_t rtcm;                // RTCM data
    protected: stream_t stream_o;          // Streaming interface (in)
    protected: stream_t stream_i;          // Streaming interface (out)
    protected: tk::spline csiTropRv;       // Cubic spline
    protected: tk::spline csiTropRd;       // Cubic spline
    protected: tk::spline csiTropNv;       // Cubic spline
    protected: tk::spline csiTropNd;       // Cubic spline
    protected: tk::spline csiTropEv;       // Cubic spline
    protected: tk::spline csiTropEd;       // Cubic spline

            // List of all systems
    protected: std::vector<GNSS> systems;  // List of GNSS Systems
    protected: common::Time bas_last;      // Last solution probe time
    protected: common::Time bas_wait;      // Solution probe wait
    protected: common::Time sol_last;      // Last solution probe time
    protected: common::Time sol_wait;      // Solution probe wait
    protected: common::Time upd_last;      // Last navigation update
    protected: common::Time upd_wait;      // Navigation update time
    protected: sensors::NoisePtr noiseP;   // Pseudorange noise
    protected: sensors::NoisePtr noiseL;   // Carrier phase noise
    protected: double error[6], lp[3];     // Error and last solution
    protected: gtime_t lt;                 // Last time

    // Default constructor
    public: GpsPlugin() : state_ptr(0)
    {
      // Initialise the RTCM object
      init_rtcm(&rtcm);
    }

    // Default destructor
    public: ~GpsPlugin() 
    {
      // Stop sensor updates
      if (this->parentSensor)
        this->parentSensor->DisconnectUpdated(this->conPtr); 

      // Close TCP stream
      strclose(&stream_i);
      strclose(&stream_o);

      // Free the RTCM data
      free_rtcm(&rtcm);
    }

    // Perturb the value with the actual error from the system
    private: double Perturb(int i, double val)
    {
      if (debug)
      {
        switch(i)
        {
          case 0: gzwarn << "ERROR: Latitude   (m)   : " << error[i] << std::endl; break;
          case 1: gzwarn << "ERROR: Longitude  (m)   : " << error[i] << std::endl; break;
          case 2: gzwarn << "ERROR: Altitude   (m)   : " << error[i] << std::endl; break;
          case 3: gzwarn << "ERROR: Velocity E (m/s) : " << error[i] << std::endl; break;
          case 4: gzwarn << "ERROR: Velocity N (m/s) : " << error[i] << std::endl; break;
          case 5: gzwarn << "ERROR: Velocity U (m/s) : " << error[i] << std::endl; break;
        }
      }
      return val + (i < 6 ? error[i] : 0.0);
    }

    // Generate a measurement to satellite id and return the elevation to the satellite 
    private: void Process()
    {
      double dt, ep[6], tp[3];   // Current dt, epoch, position, ref position and tmp
      gtime_t ct;                // Current time (GPST)

      // Determine the estimated ECEF position
      if (state_ptr < 512) 
        state_buff[state_ptr] = '\0';
      std::string str = state_buff;
      size_t start = str.find_first_not_of(" "), end = start;
      int pos = 0;
      while (start != std::string::npos)
      {
        end = str.find(" ", start);
        std::string tmp = str.substr(start, end-start);
        switch (pos)
        {
          case STATE_DATE: sscanf(tmp.c_str(),"%lf/%lf/%lf",&ep[0],&ep[1],&ep[2]); break; 
          case STATE_TIME: sscanf(tmp.c_str(),"%lf:%lf:%lf",&ep[3],&ep[4],&ep[5]); break; 
          case STATE_X: sscanf(tmp.c_str(),"%lf",&tp[0]); break;
          case STATE_Y: sscanf(tmp.c_str(),"%lf",&tp[1]); break;
          case STATE_Z: sscanf(tmp.c_str(),"%lf",&tp[2]); break;
        }
        start = str.find_first_not_of(" ", end);
        pos++;
      }

      // Convert the epoch to a UTC time
      ct = epoch2time(ep);
      dt = timediff(ct,lt);

      // In an ideal world there'd be a spherical2local function in gazebo, which would allow us
      // to obtain the LTP cartesian coordinate for the estimated position. 

      // Get the reference position
      double r_wgs84[3], r_ecef[3], r_enu[3];

#if GAZEBO_MAJOR_VERSION > 6
      r_wgs84[0] = this->worldPtr->GetSphericalCoordinates()->LatitudeReference().Radian();
      r_wgs84[1] = this->worldPtr->GetSphericalCoordinates()->LongitudeReference().Radian();
#else
      r_wgs84[0] = this->worldPtr->GetSphericalCoordinates()->GetLatitudeReference().Radian();
      r_wgs84[1] = this->worldPtr->GetSphericalCoordinates()->GetLongitudeReference().Radian();
#endif

      r_wgs84[2] = this->worldPtr->GetSphericalCoordinates()->GetElevationReference();

      // Get the estimated position and convert to ENU coordinates
      double e_wgs84[3], e_ecef[3], e_enu[3];
      e_wgs84[0] = D2R*tp[0];
      e_wgs84[1] = D2R*tp[1];
      e_wgs84[2] = tp[2];
      pos2ecef(e_wgs84,e_ecef);
      ecef2enu(r_wgs84,e_ecef,e_enu);

      // Get the truth position and convert to ENU coordinates
      double t_wgs84[3], t_ecef[3], t_enu[3];

#if GAZEBO_MAJOR_VERSION > 6
      physics::LinkPtr parentLink = boost::dynamic_pointer_cast<physics::Link>(
        this->worldPtr->GetEntity(this->parentSensor->ParentName())
      );
      ignition::math::Pose3d pose = this->parentSensor->Pose() + parentLink->GetWorldPose().Ign();
      math::Vector3 enu = this->worldPtr->GetSphericalCoordinates()->SphericalFromLocal(pose.Pos());
#else
      physics::LinkPtr parentLink = boost::dynamic_pointer_cast<physics::Link>(
        this->worldPtr->GetEntity(this->parentSensor->GetParentName())
      );
      math::Pose pose = this->parentSensor->GetPose() + parentLink->GetWorldPose();
      math::Vector3 enu = this->worldPtr->GetSphericalCoordinates()->SphericalFromLocal(pose.pos);
#endif

      t_wgs84[0] = D2R*enu.x;
      t_wgs84[1] = D2R*enu.y;
      t_wgs84[2] = enu.z;
      pos2ecef(t_wgs84,t_ecef);
      ecef2enu(r_wgs84,t_ecef,t_enu);
      
      // Get the truthful velocity in the ENU frame
      double v_enu[3];

#if GAZEBO_MAJOR_VERSION > 6
      math::Vector3 vel = this->worldPtr->GetSphericalCoordinates()->GlobalFromLocal(
        parentLink->GetWorldLinearVel(this->parentSensor->Pose().Pos()).Ign()
      );
#else
      math::Vector3 vel = this->worldPtr->GetSphericalCoordinates()->GlobalFromLocal(
        parentLink->GetWorldLinearVel(this->parentSensor->GetPose().pos)
      );
#endif

      v_enu[0] = vel.x;
      v_enu[1] = vel.y;
      v_enu[2] = vel.z;

      // Copy over the position and velocity errors
      for (int i = 0; i < 6; i++)
        error[i] = (i < 3 ? e_enu[i] - t_enu[i] : (e_enu[i-3] - lp[i-3]) / dt - v_enu[i-3]);
       
      // Backup the old values for next velocity caluclation
      lt = ct;
      for (int i = 0; i < 3; i++) 
        lp[i] = e_enu[i];
    } 

    // Transmit an observble for a given satellite system
    protected: common::Time TxObs(const common::Time &t_sim, const GNSS &gnss, int sync)
    {
      // Preallocation for speed
      double pr[3], pv[3], rr[6], rs[6], es[3], u[3], dts[2], pos, azel[2], r, dr, ro, var, v[3]; // Geometry
      double idelay, ivar, tdelay, tvar;                                                            // ION delay
      int svh;                                                                                      // Health

      // Get the reference position (latitude, longitude, altitude)
      double r_wgs84[3];

#if GAZEBO_MAJOR_VERSION > 6
      r_wgs84[0] = this->worldPtr->GetSphericalCoordinates()->LatitudeReference().Radian();
      r_wgs84[1] = this->worldPtr->GetSphericalCoordinates()->LongitudeReference().Radian();
#else
      r_wgs84[0] = this->worldPtr->GetSphericalCoordinates()->GetLatitudeReference().Radian();
      r_wgs84[1] = this->worldPtr->GetSphericalCoordinates()->GetLongitudeReference().Radian();
#endif

      r_wgs84[2] = this->worldPtr->GetSphericalCoordinates()->GetElevationReference();

      // Determine the true ECEF position (rr) of the receiver
#if GAZEBO_MAJOR_VERSION > 6
      physics::LinkPtr parentLink = boost::dynamic_pointer_cast<physics::Link>(
        this->worldPtr->GetEntity(this->parentSensor->ParentName())
      );
      ignition::math::Pose3d pose = this->parentSensor->Pose() + parentLink->GetWorldPose().Ign();
      math::Vector3 enu = this->worldPtr->GetSphericalCoordinates()->SphericalFromLocal(pose.Pos());
#else
      physics::LinkPtr parentLink = boost::dynamic_pointer_cast<physics::Link>(
        this->worldPtr->GetEntity(this->parentSensor->GetParentName())
      );
      math::Pose pose = this->parentSensor->GetPose() + parentLink->GetWorldPose();
      math::Vector3 enu = this->worldPtr->GetSphericalCoordinates()->SphericalFromLocal(pose.pos);
#endif

      pr[0] = D2R*enu.x;
      pr[1] = D2R*enu.y;
      pr[2] = enu.z;
      pos2ecef(pr,rr); 

      // Work out ECEF velocity by adding 1s onto local position, transforming then subtracting
#if GAZEBO_MAJOR_VERSION > 6
      ignition::math::Vector3d vel = this->worldPtr->GetSphericalCoordinates()->GlobalFromLocal(
        parentLink->GetWorldLinearVel(this->parentSensor->Pose().Pos()).Ign()
      );
      enu = this->worldPtr->GetSphericalCoordinates()->SphericalFromLocal(pose.Pos() + vel);
#else
      math::Vector3 vel = this->worldPtr->GetSphericalCoordinates()->GlobalFromLocal(
        parentLink->GetWorldLinearVel(this->parentSensor->GetPose().pos)
      );
      enu = this->worldPtr->GetSphericalCoordinates()->SphericalFromLocal(pose.pos + vel); 
#endif

      pv[0] = D2R*enu.x;
      pv[1] = D2R*enu.y;
      pv[2] = enu.z;
      pos2ecef(pv,v); 
      rr[3] = v[0] - rr[0];
      rr[4] = v[1] - rr[1];
      rr[5] = v[2] - rr[2];

      // Gazebo time -> UTC -> GPST
      double t_utc = ((double)t_sim.sec) + ((double)t_sim.nsec) / 1e9;
      rtcm.time.time = round(t_utc);
      rtcm.time.sec  = t_utc - (double) rtcm.time.time;
      rtcm.time = utc2gpst(rtcm.time);

      // Kill all existing observations
      freeobs(&(rtcm.obs));

      // Iterate over all stellites
      for (int prn = gnss.prn_min; prn <= gnss.prn_max; prn++)
      {
        // Create a new observation
        obsd_t obs;

        // Get the id of the satellite
        int id = satno(gnss.sys, prn);

        // Set all of the times
        gtime_t t = obs.time = rtcm.time;

        // Solve the light-time equation to 1mm pseudorange error in order to determine
        // the true position of the satellite (rs) at the time of transmission
        r = 0;
        do
        {
          ro = r;
          if (!satpos(t,t,id,EPHOPT_PREC,&pnav,rs,dts,&var,&svh))
            continue;
          r = geodist(rs, rr, u);                         // Includes sagnac effect
          t = timeadd(obs.time, -r/CLIGHT);               // Light-time equation
        }
        while(fabs(r-ro) > 0.001);                        // Keep going until converges to 1mm

        // If we should mask the visibility of obs
        if (r==0 || (mask && satazel(pr,u,azel) < 0))
            continue;

        // Calculate doppler velocity
        v[0] = rr[3] - rs[3];
        v[1] = rr[4] - rs[4];
        v[2] = rr[5] - rs[5];
        dr = dot(u,v,3);        

        // Perturb pseudorange / doppler by the clock bias / relativistic effects
        r  -= dts[0] * CLIGHT;
        dr -= dts[1] * CLIGHT;

        // Get the L1 ionospheric path delay
        if (!iontec(obs.time, &pnav, pr, azel, 0, &idelay, &ivar))
          gzwarn << "Could not read ionospheric data" << std::endl;
        else if (ivar != 900) 
          idelay *= (FREQ1*FREQ1); // L1 -> TEC
        else
          gzwarn << "No valid ionospheric data" << std::endl;

        // Tropospheric path delay is invariant to observable
        double T = this->csiTropRv((double)obs.time.time+obs.time.sec)/1000.0;

        // Assemble the observation
        obs.sat  = id;
        obs.rcv  = 1;
        for (int i = 0; i < 3; i++)
        {
          // If a code was specified 
          if (gnss.code[i] != CODE_NONE)
          {
            // Signal to noise ratio and loss of link indicator
            obs.SNR[i] = 150 + rand() % 60; // TODO: make this something meaningful
            obs.LLI[i] = 0;                
            obs.code[i] = gnss.code[i]; 
   
            // Fundamental frequency may be based on the PRN of the satellite vehicle
            double f0 = gnss.freq[i] + gnss.bias[i] * (prn - 1);
            double I = idelay / f0 / f0;

            // Observables
            obs.P[i] = r + I + T + noiseP->Apply(0);                                  // Pseudorange (meters)
            obs.L[i] = fmod(r - I + T + noiseL->Apply(0), CLIGHT/f0) / (CLIGHT/f0);   // Phase (cycles)
            obs.D[i] = -f0/CLIGHT * dr;                                               // Doppler (Hz)
          }
        }

        // Add the observation to the given satellite
        addobsdata(&(rtcm.obs), &obs);
      }

      // Send the RTCM3 observation data on the wire
      rtcm.obsflag = 1;
      if (!gen_rtcm3(&rtcm, gnss.rtcm_obs, sync))
        gzwarn << "Problem generating OBS for SYS " << gnss.sys << std::endl;
      else if (!rtcm.nbyte)
        gzwarn << "No OBS bytes generated for SYS " << gnss.sys << std::endl;
      strwrite(&stream_o, rtcm.buff, rtcm.nbyte);

      // Return the time
      return t_sim;
    }


    // Transmit an observble for a given satellite system (WORKING)
    protected: common::Time TxNav(const common::Time &t_sim, const GNSS &gnss, int sync)
    {
      // Preallocation for speed
      double pr[3], rr[6], rs[6], u[3], dts[2], pos, var; int svh;

      // Determine the true ECEF position of the receiver
#if GAZEBO_MAJOR_VERSION > 6
      physics::LinkPtr parentLink = boost::dynamic_pointer_cast<physics::Link>(
        this->worldPtr->GetEntity(this->parentSensor->ParentName())
      );
      ignition::math::Pose3d pose = this->parentSensor->Pose() + parentLink->GetWorldPose().Ign();
      math::Vector3 enu = this->worldPtr->GetSphericalCoordinates()->SphericalFromLocal(pose.Pos());
#else
      physics::LinkPtr parentLink = boost::dynamic_pointer_cast<physics::Link>(
        this->worldPtr->GetEntity(this->parentSensor->GetParentName())
      );
      math::Pose pose = this->parentSensor->GetPose() + parentLink->GetWorldPose();
      math::Vector3 enu = this->worldPtr->GetSphericalCoordinates()->SphericalFromLocal(pose.pos);
#endif

      pr[0] = D2R*enu.x;
      pr[1] = D2R*enu.y;
      pr[2] = enu.z;
      pos2ecef(pr,rr); 

      // Gazebo time -> UTC -> GPST
      double t_utc = ((double)t_sim.sec) + ((double)t_sim.nsec) / 1e9;
      rtcm.time.time = round(t_utc);
      rtcm.time.sec  = t_utc - (double) rtcm.time.time;
      rtcm.time = utc2gpst(rtcm.time);

      // Iterate over all stellites
      for (int prn = gnss.prn_min; prn <= gnss.prn_max; prn++)
      {
        // Set the time and satellite number
        int id = satno(gnss.sys,prn);

        // Mark the ephemeride to be broadcast
        rtcm.ephsat = id;
        
        // Get the satellite position and clock (if we can't retrieve it, then it's not a valid sat)
        if (!satpos(rtcm.time,rtcm.time,id,EPHOPT_BRDC,&(rtcm.nav),rs,dts,&var,&svh))
          continue;

        // Work out the geometric distance from satellite to receiver + LOS unit vector
        geodist(rs, rr, u);

        // If we should mask the visibility of nav
        if (mask && satazel(pr,u,NULL) < 0)
            continue;

        // Send the NAV on the wire
        if (!gen_rtcm3(&rtcm, gnss.rtcm_nav, sync))
          gzwarn << "Problem generating NAV for PRN " << prn << " of SYS " << gnss.sys << std::endl;
        else if (!rtcm.nbyte)
          gzwarn << "No NAV bytes generated for PRN " << prn << " of SYS " << gnss.sys << std::endl;
        strwrite(&stream_o, rtcm.buff, rtcm.nbyte);
      }

      // Return the time in RTCM data
      return t_sim;
    }

    // Transmit an observble for a given satellite system
    protected: common::Time UpNav(const common::Time &t_sim)
    {
      // Gazebo time -> UTC -> GPST
      double t_utc = ((double)t_sim.sec) + ((double)t_sim.nsec) / 1e9;
      rtcm.time.time = round(t_utc);
      rtcm.time.sec  = t_utc - (double) rtcm.time.time;
      rtcm.time = utc2gpst(rtcm.time);

      // Clear the GPS/CMP/GAL/QZS and GLO navigation data
      freenav(&(rtcm.nav),SYS_GPS|SYS_GLO);

      // KEPLERIAN-STYLE NAV /////////////////////////////////////////////////////////

      // Allocate memory
      rtcm.nav.n = MAXSAT;
      rtcm.nav.eph = (eph_t*) malloc(sizeof(eph_t)*MAXSAT);

      // Insert
      int idx = 0;
      for (int prn = MINPRNGPS; prn <= MAXPRNGPS; prn++)
      {
        int id = satno(SYS_GPS, prn);
        for (int i = 0; i < bnav.n; i++)
        {
          if (bnav.eph[i].sat==id && fabs(timediff(rtcm.time, bnav.eph[i].toe)) < MAXDTOE)
            rtcm.nav.eph[id-1] = bnav.eph[i];   
        }
      }
      for (int prn = MINPRNGAL; prn <= MAXPRNGAL; prn++)
      {
        int id = satno(SYS_GAL, prn);
        for (int i = 0; i < bnav.n; i++)
        {
          if (bnav.eph[i].sat==id && fabs(timediff(rtcm.time, bnav.eph[i].toe)) < MAXDTOE)
            rtcm.nav.eph[id-1] = bnav.eph[i];     
        }
      }
      for (int prn = MINPRNQZS; prn <= MAXPRNQZS; prn++)
      {
        int id = satno(SYS_QZS, prn);
        for (int i = 0; i < bnav.n; i++)
        {
          if (bnav.eph[i].sat==id && fabs(timediff(rtcm.time, bnav.eph[i].toe)) < MAXDTOE)
            rtcm.nav.eph[id-1] = bnav.eph[i];     
        }
      }
      for (int prn = MINPRNCMP; prn <= MAXPRNCMP; prn++)
      {
        int id = satno(SYS_CMP, prn);
        for (int i = 0; i < bnav.n; i++)
        {
          if (bnav.eph[i].sat==id && fabs(timediff(rtcm.time, bnav.eph[i].toe)) < MAXDTOE)
            rtcm.nav.eph[id-1] = bnav.eph[i];     
        }
      }

      // ECEF-STYLE NAV //////////////////////////////////////////////////////////////

      // Allocate memory      
      rtcm.nav.ng = NSATGLO;
      rtcm.nav.geph = (geph_t*) malloc(sizeof(geph_t)*NSATGLO);

      // Insert
      for (int prn = MINPRNGLO; prn <= MAXPRNGLO; prn++)
      {
        int id = satno(SYS_GLO, prn);
        for (int i = 0; i < bnav.ng; i++)
        {
          if (bnav.geph[i].sat==id && fabs(timediff(rtcm.time, bnav.geph[i].toe)) < MAXDTOE_GLO)
            rtcm.nav.geph[prn-1] = bnav.geph[i];    
        }
      }

      // Success
      return t_sim;
    }

    // Transmit an observble for a given satellite system
    protected: common::Time RxSol(const common::Time &t_sim)
    {
      // Process a response from the server into a max 256 size buffer
      char buff[256];
      int n = strread(&stream_i, (unsigned char*)buff, 256);
      if (n)
      {
        // If there is a line termination we need to flush the buffer
        char *pch = strchr(buff,'\n');
        if (pch != NULL)
        {
          // Append data to the buffer pre-flush
          int l = (int)(pch - buff);
          memcpy(state_buff + state_ptr, buff, l);
          state_ptr += l;

          // Some data has arrived! Process it!
          Process();

          // Reset the state pointer
          state_ptr = 0;
          n -= l;
        }
        else
          pch = buff;

        // Always append the data
        memcpy(state_buff + state_ptr, pch, n);
        state_ptr += n;
      }

      // Return the time in RTCM data
      return t_sim;
    }

    // Transmit an observble for a given satellite system
    protected: common::Time TxBas(const common::Time &t_sim)
    {
      // Set the basic information for the base station
      strcpy(rtcm.sta.name,"Gazebo");
      strcpy(rtcm.sta.marker,"0");
      strcpy(rtcm.sta.antdes,"Simulated");
      strcpy(rtcm.sta.antdes,"0.0.1");
      strcpy(rtcm.sta.rectype,"Simulated");
      strcpy(rtcm.sta.recver,"0.0.1");
      strcpy(rtcm.sta.recsno,"0");
      rtcm.sta.antsetup = 1;
      rtcm.sta.itrf = 1;
      rtcm.sta.deltype = 0;
      rtcm.sta.hgt = 0;

      // Set the station position and delta
      double pr[3], rr[6];

#if GAZEBO_MAJOR_VERSION > 6
      physics::LinkPtr parentLink = boost::dynamic_pointer_cast<physics::Link>(
        this->worldPtr->GetEntity(this->parentSensor->ParentName())
      );
      ignition::math::Pose3d pose = this->parentSensor->Pose() + parentLink->GetWorldPose().Ign();
      math::Vector3 enu = this->worldPtr->GetSphericalCoordinates()->SphericalFromLocal(pose.Pos());
#else
      physics::LinkPtr parentLink = boost::dynamic_pointer_cast<physics::Link>(
        this->worldPtr->GetEntity(this->parentSensor->GetParentName())
      );
      math::Pose pose = this->parentSensor->GetPose() + parentLink->GetWorldPose();
      math::Vector3 enu = this->worldPtr->GetSphericalCoordinates()->SphericalFromLocal(pose.pos);
#endif

      pr[0] = D2R*enu.x;
      pr[1] = D2R*enu.y;
      pr[2] = enu.z;
      pos2ecef(pr,rr); 
      for (int i = 0; i < 3; i++)
      {
        rtcm.sta.pos[i] = rr[i];
        rtcm.sta.del[i] = 0.0;
      }

      // Send the  base station parameters on the wire
      if (!gen_rtcm3(&rtcm, 1005, 0))
        gzwarn << "Problem generating BAS" << std::endl;
      else if (!rtcm.nbyte)
        gzwarn << "No BAS bytes generated" << std::endl;
      strwrite(&stream_o, rtcm.buff, rtcm.nbyte);

      // Return the time in RTCM data
      return t_sim;
    }

    // Called on every world tick to see what the plugin needs to do
    protected: void EventScheduler(const common::UpdateInfo &_info)
    {
      // Update the base station parameters
      if (bas_wait.Double() > 0 && _info.simTime > bas_last + bas_wait)
        bas_last = TxBas(_info.simTime);        

      // Update the navs -- every 15 minutes (must be done first)
      if (upd_wait.Double() > 0 && _info.simTime > upd_last + upd_wait)
        upd_last = UpNav(_info.simTime);        

      // Send NAVs out at a rate specified by the constellation
      for (std::vector<GNSS>::iterator it = systems.begin(); it != systems.end(); it++)
        if (it->nav_wait.Double() > 0 && _info.simTime > it->nav_last + it->nav_wait) 
          it->nav_last = TxNav(_info.simTime, *it, (it == systems.end() - 1 ? 0 : 1));
      
      // Observations and solutions
      if (sol_wait.Double() > 0 && _info.simTime > sol_last + sol_wait)
      {
        // Generate observations
        for (std::vector<GNSS>::iterator it = systems.begin(); it != systems.end(); it++)
          sol_last = TxObs(_info.simTime, *it, (it == systems.end() - 1 ? 0 : 1));
        
        // Receive colution
        sol_last = RxSol(_info.simTime);
      }
    }

    // Called once on load to parse tropospheric file
    protected: bool ParseTropFile(const std::string &_file)
    {
      // Read the base file
      std::string line;
      std::ifstream myfile(_file.c_str());
      bool tstate = false;
      bool sstate = false;
      
      // Ignor
      if (!myfile) 
        return false;
      
      // Vectors for data storage
      std::vector<double> tropT;  // Tropospheric path delay time
      std::vector<double> tropRv; // Tropospheric path delay val
      std::vector<double> tropNv; // Tropospheric path delay N grad
      std::vector<double> tropEv; // Tropospheric path delay E grad
      std::vector<double> tropRd; // Tropospheric path delay val std
      std::vector<double> tropNd; // Tropospheric path delay N grad std
      std::vector<double> tropEd; // Tropospheric path delay E grad std

      // Rad line-by-line
      while (getline(myfile,line))
      {
        // Station coordinates
        if (line.compare("+TROP/STA_COORDINATES")==0)
        {
          getline(myfile,line); // Gobble header
          sstate = true;        // Start receiving data
        }
        if (line.compare("-TROP/STA_COORDINATES")==0)
          sstate = false;       // Stop receiving data
        else if (sstate)
        {
          // Extract the data from the line
          std::string site, pt, t, sys, remark;
          int soln;
          double sta_x, sta_y, sta_z;
          std::istringstream iss(line);
          iss >> site >> pt >> soln >> t >> sta_x >> sta_y >> sta_z 
            >> sys >> remark;
        }

        // Measurements
        if (line.compare("+TROP/SOLUTION")==0)
        {
          getline(myfile,line); // Gobble header
          tstate = true;        // Start receiving data
        }
        if (line.compare("-TROP/SOLUTION")==0)
          tstate = false;       // Stop receiving data
        else if (tstate)
        {
          // Modify character to make life easier
          std::replace(line.begin(), line.end(), ':', ' ');

          // Extract the data from the line
          std::string station;
          int ey, ed, es;
          double trotot, stdev, tgntot, tgndev, tgetot, tgedev, tim;
          std::istringstream iss(line);
          iss >> station >> ey >> ed >> es >> trotot >> stdev >> 
            tgntot >> tgndev >> tgetot >> tgedev;

          // Midnight at the begining of January 1, YY plus the number of
          // seconds that have elapsed since that point
          double ep[6];
          ep[0] = 2000.0 + (double) ey;
          ep[1] = 1;
          ep[2] = 1;
          ep[3] = 0;
          ep[4] = 0;
          ep[5] = 0;
          gtime_t utc = timeadd(epoch2time(ep),(double)ed*86400.0+(double)es);
          gtime_t gps = utc2gpst(utc);

          // Push on the data
          tropT.push_back((double)gps.time + gps.sec);
          tropRv.push_back(trotot);
          tropRd.push_back(stdev);
          tropNv.push_back(tgntot);
          tropNd.push_back(tgndev);
          tropEv.push_back(tgetot);
          tropEd.push_back(tgedev);
        }
      }

      // Close file
      myfile.close();
      
      // Initialise the cubis spline interpolators
      this->csiTropRv.set_points(tropT,tropRv);
      this->csiTropRd.set_points(tropT,tropRd);
      this->csiTropNv.set_points(tropT,tropNv);
      this->csiTropNd.set_points(tropT,tropNd);
      this->csiTropEv.set_points(tropT,tropEv);
      this->csiTropEd.set_points(tropT,tropEd);

      // Success
      return true;
    }

    // Called on initialisation
    public: void Load(sensors::SensorPtr sensor, sdf::ElementPtr root)
    {
      // Precise ephemerides
      if (root->HasElement("peph"))
      {
        std::string tmp;
        root->GetElement("peph")->GetValue()->Get(tmp);
        readsp3(common::SystemPaths::Instance()->FindFileURI(tmp).c_str(), &pnav, 1);
        gzlog << "Read final ephemerides" << std::endl;
      }

      // Precise clocks
      if (root->HasElement("pclk"))
      {
        std::string tmp;
        root->GetElement("pclk")->GetValue()->Get(tmp);
        if (!readrnxc(common::SystemPaths::Instance()->FindFileURI(tmp).c_str(), &pnav))
          gzwarn << "Could not read final clocks" << std::endl;
        else
          gzlog << "Read final clocks" << std::endl;
      }

      // IONEX file
      if (root->HasElement("ftec"))
      {
        std::string tmp;
        root->GetElement("ftec")->GetValue()->Get(tmp);
        readtec(common::SystemPaths::Instance()->FindFileURI(tmp).c_str(), &pnav, 1);
        gzlog << "Read IONEX file" << std::endl;
      }

      // Broadcast ephemerides
      if (root->HasElement("beph"))
      {
        std::string tmp;
        root->GetElement("beph")->GetValue()->Get(tmp);
        if (!readrnx(common::SystemPaths::Instance()->FindFileURI(tmp).c_str(), 1, "", NULL, &bnav, NULL))
          gzerr << "Could not read broadcast ephemerides file: " << tmp << std::endl;
        else
          gzlog << "Read broadcast ephemerides" << std::endl;
      }

      // Get the humidity
      if (root->HasElement("trop"))
      {
        std::string tmp;
        root->GetElement("trop")->GetValue()->Get(tmp);
        if (this->ParseTropFile(common::SystemPaths::Instance()->FindFileURI(tmp).c_str()))
          gzlog << "Read tropospheric zenith path delay file: " << tmp <<  std::endl;
        else
          gzwarn << "Tropospheric zenith path delay file could not be read: " << tmp <<  std::endl;
      }

      // Summary
      gzlog << "Number of precise eph: " << pnav.ne << std::endl;
      gzlog << "Number of KEPLER beph: " << bnav.n  << std::endl; 
      gzlog << "Number of GLONAS beph: " << bnav.ng << std::endl;
      gzlog << "Number of IONEX tdata: " << pnav.nt << std::endl;

      // Configure the GNSS systems
      if (root->HasElement("constellations"))
      {
        sdf::ElementPtr el_con = root->GetElement("constellations");
        sdf::ElementPtr el_sys = el_con->GetFirstElement();
        do
        {
          // New GNSS type
          GNSS gnss;

          // Get the system ID, check that it's OK and set RTCM codes
          if (!el_sys->HasAttribute("id")) gzerr << "No sys attribute for <system>";
          std::string sys = el_sys->Get<std::string>("id");
               if (sys=="GPS") { gnss.sys=SYS_GPS; gnss.rtcm_nav=1019; gnss.rtcm_obs=1075; gnss.prn_min=MINPRNGPS; gnss.prn_max=MAXPRNGPS; gnss.nav_wait=common::Time(1.0); }
          else if (sys=="GLO") { gnss.sys=SYS_GLO; gnss.rtcm_nav=1020; gnss.rtcm_obs=1085; gnss.prn_min=MINPRNGLO; gnss.prn_max=MAXPRNGLO; gnss.nav_wait=common::Time(1.0); }
          else if (sys=="QZS") { gnss.sys=SYS_QZS; gnss.rtcm_nav=1044; gnss.rtcm_obs=1115; gnss.prn_min=MINPRNQZS; gnss.prn_max=MAXPRNQZS; gnss.nav_wait=common::Time(1.0); }
          else if (sys=="GAL") { gnss.sys=SYS_GAL; gnss.rtcm_nav=1045; gnss.rtcm_obs=1095; gnss.prn_min=MINPRNGAL; gnss.prn_max=MAXPRNGAL; gnss.nav_wait=common::Time(1.0); }
          else if (sys=="CMP") { gnss.sys=SYS_CMP; gnss.rtcm_nav=1047; gnss.rtcm_obs=1125; gnss.prn_min=MINPRNCMP; gnss.prn_max=MAXPRNCMP; gnss.nav_wait=common::Time(1.0); }
          else gzerr << "The sys attribute '" << sys << "'' for <system> is invalid. Should be GPS, GAL, GLO, CMP, QZS";
          gzlog << "Processing " << sys << std::endl;

          // The NAV timing depends on the constellation
          gnss.nav_last = common::Time(0);

          // Get the NAV update rate
          bool valid = false;
          for (int i = 0; i < 3; i++)
          {
            // Extract the code
            std::ostringstream oss; oss << "code" << (i+1);
            if (!el_sys->HasAttribute(oss.str())) 
              continue;
            std::string cod = el_sys->Get<std::string>(oss.str());
                 if (cod=="L1C") { gnss.code[i]=CODE_L1C; } // GPS, GLO, GAL, QZS
            else if (cod=="L2C") { gnss.code[i]=CODE_L2C; } // GPS, GLO, QZS
            else if (cod=="L6C") { gnss.code[i]=CODE_L6C; } // GAL
            else if (cod=="L5I") { gnss.code[i]=CODE_L5I; } // GPS, GAL, QZS
            else if (cod=="L5Q") { gnss.code[i]=CODE_L5Q; } // GPS, GAL, QZS
            else if (cod=="L1I") { gnss.code[i]=CODE_L1I; } // CMP
            else if (cod=="L1Q") { gnss.code[i]=CODE_L1Q; } // CMP
            else if (cod=="L2I") { gnss.code[i]=CODE_L2I; } // CMP
            else if (cod=="L2Q") { gnss.code[i]=CODE_L2Q; } // CMP
            else if (cod=="L3I") { gnss.code[i]=CODE_L3I; } // CMP
            else if (cod=="L3Q") { gnss.code[i]=CODE_L3Q; } // CMP
            else gzerr << "The code" << i+1 << " attribute '" << cod << "'' for <system> is invalid. Should be L1C, L2C, L5Q, L5I";
            valid = true;

            // Provide the freuqency / bias mapping
                 if (gnss.sys==SYS_GPS && gnss.code[i]==CODE_L1C) { gnss.freq[i]=FREQ1;     gnss.bias[i]=0.0; }           // GPS
            else if (gnss.sys==SYS_GPS && gnss.code[i]==CODE_L2C) { gnss.freq[i]=FREQ2;     gnss.bias[i]=0.0; }
            else if (gnss.sys==SYS_GPS && gnss.code[i]==CODE_L5I) { gnss.freq[i]=FREQ5;     gnss.bias[i]=0.0; }
            else if (gnss.sys==SYS_GPS && gnss.code[i]==CODE_L5Q) { gnss.freq[i]=FREQ5;     gnss.bias[i]=0.0; }
            else if (gnss.sys==SYS_QZS && gnss.code[i]==CODE_L1C) { gnss.freq[i]=FREQ1;     gnss.bias[i]=0.0; }           // QZS
            else if (gnss.sys==SYS_QZS && gnss.code[i]==CODE_L2C) { gnss.freq[i]=FREQ2;     gnss.bias[i]=0.0; }
            else if (gnss.sys==SYS_QZS && gnss.code[i]==CODE_L5I) { gnss.freq[i]=FREQ5;     gnss.bias[i]=0.0; }
            else if (gnss.sys==SYS_QZS && gnss.code[i]==CODE_L5Q) { gnss.freq[i]=FREQ5;     gnss.bias[i]=0.0; }
            else if (gnss.sys==SYS_GAL && gnss.code[i]==CODE_L1C) { gnss.freq[i]=FREQ1;     gnss.bias[i]=0.0; }           // GAL
            else if (gnss.sys==SYS_GAL && gnss.code[i]==CODE_L6C) { gnss.freq[i]=FREQ6;     gnss.bias[i]=0.0; }
            else if (gnss.sys==SYS_GAL && gnss.code[i]==CODE_L5I) { gnss.freq[i]=FREQ5;     gnss.bias[i]=0.0; }
            else if (gnss.sys==SYS_GAL && gnss.code[i]==CODE_L5Q) { gnss.freq[i]=FREQ5;     gnss.bias[i]=0.0; }
            else if (gnss.sys==SYS_GLO && gnss.code[i]==CODE_L1C) { gnss.freq[i]=FREQ1_GLO; gnss.bias[i]=DFRQ1_GLO; } // GLO
            else if (gnss.sys==SYS_GLO && gnss.code[i]==CODE_L2C) { gnss.freq[i]=FREQ2_GLO; gnss.bias[i]=DFRQ2_GLO; }
            else if (gnss.sys==SYS_CMP && gnss.code[i]==CODE_L1I) { gnss.freq[i]=FREQ1_CMP; gnss.bias[i]=0.0; }       // CMP
            else if (gnss.sys==SYS_CMP && gnss.code[i]==CODE_L1Q) { gnss.freq[i]=FREQ1_CMP; gnss.bias[i]=0.0; }
            else if (gnss.sys==SYS_CMP && gnss.code[i]==CODE_L2I) { gnss.freq[i]=FREQ2_CMP; gnss.bias[i]=0.0; }
            else if (gnss.sys==SYS_CMP && gnss.code[i]==CODE_L2Q) { gnss.freq[i]=FREQ2_CMP; gnss.bias[i]=0.0; }
            else if (gnss.sys==SYS_CMP && gnss.code[i]==CODE_L3I) { gnss.freq[i]=FREQ3_CMP; gnss.bias[i]=0.0; }
            else if (gnss.sys==SYS_CMP && gnss.code[i]==CODE_L3Q) { gnss.freq[i]=FREQ3_CMP; gnss.bias[i]=0.0; }
            else { gnss.code[i]=CODE_NONE; }
          }

          // If this is a complete system, push onto the stack
          systems.push_back(gnss);
        }
        while(el_sys = el_sys->GetNextElement());
      }
      else
        gzerr << "You must specify at least one <system> for the GNSS plugin\n";

      //////////////////////////////////////////////////////////////////////////////////////////////////

      // Code phase noise
      if (root->HasElement("code"))
        noiseP = sensors::NoiseFactory::NewNoiseModel(root->GetElement("code")->GetElement("noise"));
      else
        gzerr << "You must specify a <code> element for the GNSS plugin\n";

      // Carrier phase noise
      if (root->HasElement("code"))
        noiseL = sensors::NoiseFactory::NewNoiseModel(root->GetElement("carr")->GetElement("noise"));
      else
        gzerr << "You must specify a <carr> element for the GNSS plugin\n";

      //////////////////////////////////////////////////////////////////////////////////////////////////

      // Start a streaming server with one output stream
      if (root->HasElement("rtcmport"))
      {
        std::string tmp;
        root->GetElement("rtcmport")->GetValue()->Get(tmp);
        strinit(&stream_o);
        if (!stropen(&stream_o, STR_TCPSVR, STR_MODE_W, tmp.c_str()))
          gzerr << "Could not open output TCP stream" << std::endl;
      }
      else gzerr << "Must specify a <rtcmport> value" << std::endl;

      if (root->HasElement("solnport"))
      {
        std::string tmp;
        root->GetElement("solnport")->GetValue()->Get(tmp);
        strinit(&stream_i);
        if (!stropen(&stream_i, STR_TCPSVR, STR_MODE_R, tmp.c_str()))
          gzerr << "Could not open input TCP stream" << std::endl;
      }
      else gzerr << "Must specify a <solnport> value" << std::endl;

      //////////////////////////////////////////////////////////////////////////////////////////////////

      // Get the parent sensor and the world pointer
#if GAZEBO_MAJOR_VERSION > 6
      this->parentSensor = std::dynamic_pointer_cast<sensors::GpsSensor>(sensor);
      this->worldPtr = physics::get_world(this->parentSensor->WorldName());
#else
      this->parentSensor = boost::dynamic_pointer_cast<sensors::GpsSensor>(sensor);
      this->worldPtr = physics::get_world(this->parentSensor->GetWorldName());
#endif

      // Get the debug
      if (root->HasElement("debug")) 
        root->GetElement("debug")->GetValue()->Get(debug);

      // Get the mask
      if (root->HasElement("mask")) 
        root->GetElement("mask")->GetValue()->Get(mask);
      else
        gzerr << "You must specify a <mask> element for the GNSS plugin" << std::endl;

      // Get the SOL timers
      if (root->HasElement("base")) 
      {
        double t;
        root->GetElement("base")->GetValue()->Get(t);
        bas_wait = common::Time(1.0/t);  
        bas_last = common::Time(0);
      }
      else
        gzerr << "You must specify a <bas> element for the GNSS plugin" << std::endl;

      // Get the SOL timers
      if (root->HasElement("freq")) 
      {
        double t;
        root->GetElement("freq")->GetValue()->Get(t);
        sol_wait = common::Time(1.0/t);  
        sol_last = common::Time(0);
        upd_wait = common::Time(10.0);  
        upd_last = common::Time(0);
      }
      else
        gzerr << "You must specify a <freq> element for the GNSS plugin" << std::endl;

      // Update satellite positions every time tick
      this->conPtr = event::Events::ConnectWorldUpdateBegin(
        boost::bind(&GpsPlugin::EventScheduler, this, _1));

      // Setup the noise perturbation for the sensor {pe, pn, pu, ve, vn, vu}
#if GAZEBO_MAJOR_VERSION > 6
      this->parentSensor->Noise(sensors::GPS_POSITION_LATITUDE_NOISE_METERS)->SetCustomNoiseCallback(
        boost::bind(&GpsPlugin::Perturb,this,0,_1));
      this->parentSensor->Noise(sensors::GPS_POSITION_LONGITUDE_NOISE_METERS)->SetCustomNoiseCallback(
        boost::bind(&GpsPlugin::Perturb,this,1,_1));
      this->parentSensor->Noise(sensors::GPS_POSITION_ALTITUDE_NOISE_METERS)->SetCustomNoiseCallback(
        boost::bind(&GpsPlugin::Perturb,this,2,_1));
      this->parentSensor->Noise(sensors::GPS_VELOCITY_LATITUDE_NOISE_METERS)->SetCustomNoiseCallback(
        boost::bind(&GpsPlugin::Perturb,this,3,_1));
      this->parentSensor->Noise(sensors::GPS_VELOCITY_LONGITUDE_NOISE_METERS)->SetCustomNoiseCallback(
        boost::bind(&GpsPlugin::Perturb,this,4,_1));
      this->parentSensor->Noise(sensors::GPS_VELOCITY_ALTITUDE_NOISE_METERS)->SetCustomNoiseCallback(
        boost::bind(&GpsPlugin::Perturb,this,5,_1));
#else
      this->parentSensor->GetNoise(sensors::GPS_POSITION_LATITUDE_NOISE_METERS)->SetCustomNoiseCallback(
        boost::bind(&GpsPlugin::Perturb,this,0,_1));
      this->parentSensor->GetNoise(sensors::GPS_POSITION_LONGITUDE_NOISE_METERS)->SetCustomNoiseCallback(
        boost::bind(&GpsPlugin::Perturb,this,1,_1));
      this->parentSensor->GetNoise(sensors::GPS_POSITION_ALTITUDE_NOISE_METERS)->SetCustomNoiseCallback(
        boost::bind(&GpsPlugin::Perturb,this,2,_1));
      this->parentSensor->GetNoise(sensors::GPS_VELOCITY_LATITUDE_NOISE_METERS)->SetCustomNoiseCallback(
        boost::bind(&GpsPlugin::Perturb,this,3,_1));
      this->parentSensor->GetNoise(sensors::GPS_VELOCITY_LONGITUDE_NOISE_METERS)->SetCustomNoiseCallback(
        boost::bind(&GpsPlugin::Perturb,this,4,_1));
      this->parentSensor->GetNoise(sensors::GPS_VELOCITY_ALTITUDE_NOISE_METERS)->SetCustomNoiseCallback(
        boost::bind(&GpsPlugin::Perturb,this,5,_1));
#endif

      // Make sure the parent sensor is active.
      this->parentSensor->SetActive(true);
    }

    // All sensors must be resettable
    public: void Reset() {}

    // Convenience function to reallocate observations in the RTCM data structure
    protected: static int addobsdata(obs_t *obs, const obsd_t *data)
    {
        obsd_t *obs_data;
        if (obs->nmax<=obs->n)
        {
            if (obs->nmax<=0) obs->nmax=262144; else obs->nmax*=2;
            if (!(obs_data=(obsd_t *)realloc(obs->data,sizeof(obsd_t)*obs->nmax)))
            {
                free(obs->data); obs->data=NULL; obs->n=obs->nmax=0;
                return -1;
            }
            obs->data=obs_data;
        }
        obs->data[obs->n++]=*data;
        return 1;
    }

  };

  // Resgister the plugin
  GZ_REGISTER_SENSOR_PLUGIN(GpsPlugin);

};
